export default function ({ app, redirect }) {
    //Temporary check login flag
    if (!app.$cookies.get('login')){
        redirect('/');
    }
  }