# nustjstodo



## Getting started
## Installation

### Environment setup
1. Open .env file and add api url
```bash
API_URI=http://[ip]:[port]
```
2. Run command below to setup the enviroment
```bash
$ npm install
```

(optional) 3. Change nuxtjs port, open nuxt.config.js. Find server key and change the port value 
```bash
server: {
    port : 3100
  },
```

## Running the app

```bash
$ npm run dev
```

# Access the portal
default url : http://localhost:3100/
```bash
username : user
password : password123
```

